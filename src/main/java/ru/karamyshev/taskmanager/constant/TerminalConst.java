package ru.karamyshev.taskmanager.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String EXIT = "exit";

    String COMMAND_EMPTY = "\n Line is empty.";

    String COMMAND_N_FOUND = " \n Error! Command not found.";

    String COMMAND_ABSENT = "\n Error! Command absent.";
}
