package ru.karamyshev.taskmanager;

import ru.karamyshev.taskmanager.constant.TerminalConst;

import java.util.Scanner;

public class Application implements TerminalConst {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    private static void inputCommand() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            parsArgs(command);
        }
    }

    private static void parsArgs(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) {
            chooseResponsCommand(arg);
        }
    }

    private static void parsArgs(final String... args) {
        validateArgs(args);
        for (String arg : args) {
            chooseResponsCommand(arg.trim());
        }
    }

    private static void validateArgs(final String... args) {
        if (args != null || args.length > 0) { return; }
        System.out.println(COMMAND_ABSENT);
    }

    private static void chooseResponsCommand(String arg) {
        switch (arg) {
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            case HELP:
                showHelp();
                break;
            case EXIT:
                exit();
                break;
            default:
                System.out.println(COMMAND_N_FOUND);
        }
    }

    private static void showVersion() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("\n [ABOUT]");
        System.out.println("NAME: Alexander Karamyshev");
        System.out.println("EMAIL: sanja_19.96@mail.ru");
    }

    private static void showHelp() {
        System.out.println("\n [HELP]");
        System.out.println("about - Show developer info.");
        System.out.println("version - Show version info.");
        System.out.println("help - Display terminal commands.");
        System.out.println("exit - Close application.");
    }

    private static void exit() { System.exit(0); }
}
